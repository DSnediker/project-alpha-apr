from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import CreateProjectForm

# Create your views here.


@login_required
def project_list(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "project_list": projects,
    }
    return render(request, "projects/list_projects.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    context = {
        "project_object": project,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        project_form = CreateProjectForm(request.POST)
        if project_form.is_valid():
            project = project_form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        project_form = CreateProjectForm()

    context = {
        "project_form": project_form,
    }
    return render(request, "projects/create_project.html", context)
