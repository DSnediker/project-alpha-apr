from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import CreateTaskForm
from tasks.models import Task

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        task_form = CreateTaskForm(request.POST)
        if task_form.is_valid():
            task_form.save()
            return redirect("list_projects")
    else:
        task_form = CreateTaskForm()

    context = {
        "task_form": task_form,
    }
    return render(request, "tasks/create_task.html", context)


@login_required
def task_list(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "tasks_list": tasks,
    }
    return render(request, "tasks/list_tasks.html", context)
